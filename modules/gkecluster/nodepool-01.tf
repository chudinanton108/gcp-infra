resource "google_container_node_pool" "nodepool-01" {
    name       = var.name_node_pools_1
    location   = var.node_pools_1_node_region
    node_locations = [
        var.node_pools_1_node_zone,
    ]
    cluster    = "${google_container_cluster.cluster.name}"
    node_count = var.node_pools_1_node_count

    node_config {
        preemptible  = var.preemptible
        machine_type = var.machine_type_node_pools_1
        disk_size_gb = var.node_pools_1_disk_size_gb
        metadata = {
            disable-legacy-endpoints = "true"
        }

        labels = {
            team = var.node_pools_1_label
        }

        oauth_scopes = [
            "https://www.googleapis.com/auth/devstorage.read_only",
            "https://www.googleapis.com/auth/logging.write",
            "https://www.googleapis.com/auth/monitoring",
        ]
    }

#    management {
#        auto_repair  = var.management_auto_repair
#        auto_upgrade = var.management_auto_upgrade
#    }

    autoscaling {
        min_node_count = var.node_pools_1_min_count_nodes
        max_node_count = var.node_pools_1_max_count_nodes
    }
}