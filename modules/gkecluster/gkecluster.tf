resource "google_compute_network" "network" {
  provider                = google-beta
  name                    = "${var.network}-${var.env_name}" 
  auto_create_subnetworks = "true"
}

resource "google_container_cluster" "cluster" {
  provider                 = google-beta
  name                     = "${var.cluster_name}-${var.env_name}"
  network                  = "${google_compute_network.network.id}"
  location                 = var.region # creates 1 node per zone
  node_locations = [
      var.zone,
  ]
  remove_default_node_pool = true
  initial_node_count       = var.initial_node_count
  monitoring_service       = var.monitoring_service 
  logging_service          = var.logging_service

  network_policy {
    enabled = true
  }

  maintenance_policy {
    daily_maintenance_window {
        start_time = var.daily_maintenance_window
    }
  }

  addons_config {
    istio_config {
      disabled = var.istio
      auth     = "AUTH_NONE"
    }

    http_load_balancing {
      disabled = var.http_load_balancing
    }

    horizontal_pod_autoscaling {
      disabled = var.horizontal_pod_autoscaling
    }
  }

}
